"""AdPro Assignment 1 - Group_C
------------------------------
Diego Bellon 45637
Marc Serafin 44832
Sophie Schmitz 45718
Hannah Hoehnke 44536
Kevin Cabrera 44406
Wiebke Wegener 44354
-------------------------------
This .py file defines a new Class 'Handler' with all of the neccessary methods,
to plot a correlation matrix and select specific weeks to analyze.
"""
import os
from datetime import datetime
from zipfile import ZipFile
import pandas as pd
import requests
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns


class Handler:
    """Defines class 'Handler' with seven methods.

    Attributes:
    -----------
    data_frame: pandas.DataFrame
        Pandas DataFrame as attribute of instance

    Methods:
    --------
    __init__():
        initializes the Class and sets the DataFrame as an attribute
    set_df(df_path):
        attributes the 'data_frame' to the instance
    download_url(url, save_path):
        downloads the Bike-Sharing-Dataset as a .zip file in 'downloads' file
    extract_zip(file_name):
        extract_zip: reads the .csv data in .zip file.
    create_correlation_matrix(correlation_attributes):
        create_correlation_matrix: plots a correlation matrix between the attributes given
    plot():
        allows input of specific week, which is then plotted and the selected data frame displayed
    plot_average_total_rentals():
        barplot expected rentals by month
    forecast(i_month):
        plots expected weekly rentals by hour for every month and
        synchronicities of these rentals through plotting a Fourtier transformation
    """

    def __init__(self):
        self.data_frame = pd.DataFrame()

    def set_df(self, df_path: str):
        """Method reads the .csv file and attributes it as an instance and converts the
        dataframe index to hold daily datetime information.

        Parameters:
        -----------
        df_path: string
            file path of imported file

        Returns:
        --------
        None
        """
        self.data_frame = pd.read_csv(df_path)
        self.data_frame.rename(
            columns={
                "dteday": "datetime",
                "yr": "year",
                "mnth": "month",
                "weathersit": "weather_condition",
                "hum": "humidity",
                "cnt": "count",
            },
            inplace=True,
        )
        index = pd.to_datetime(
            self.data_frame["datetime"].apply(str)
            + "-"
            + self.data_frame["hr"].apply(str),
            format="%Y-%m-%d-%H",
        )
        self.data_frame.set_index(index, inplace=True)

        self.data_frame["week"] = 0
        dates = self.data_frame["datetime"].values

        for i in range(len(self.data_frame["datetime"])):
            date_obj = datetime.strptime(
                self.data_frame["datetime"][i], "%Y-%m-%d"
            ).date()
            dates[i] = date_obj
            delta = dates[i] - dates[0]
            pd.options.mode.chained_assignment = None
            self.data_frame["week"][i] = int(delta.days / 7)

    def download_url(self, url: str, save_path: str):
        """Method will _download_ the .zip file with the data into a
        downloads/directory in the root directory of the project
        (main project directory).
        If the .zip file already exists, the method will not download it again.

        Parameters:
        -----------
        url: string
            database url
        save_path: string
            directory of the file is to be saved in

        Returns:
        --------
        None
        """
        if os.path.exists("../downloads/Bike-Sharing-Dataset.zip"):
            print("zip has already been downloaded.")
        else:
            print("downloading zip now.")
            url_request = requests.get(url, stream=True)
            with open(save_path, "wb") as f_d:
                for chunk in url_request.iter_content(chunk_size=128):
                    f_d.write(chunk)

    def extract_zip(self, file_name: str):
        """Method will read the .csv file with hourly aggregations inside
        the .zip file in the downloads/directory into the attribute 'data_frame'.

        Parameters:
        -----------
        file_name: string
            file path of .zip file.

        Returns:
        --------
        None
        """
        with ZipFile(file_name, "r") as zip:
            zip.printdir()
            print("Extracting all the files now...")
            zip.extractall("../data")
            print("Done!")

    def create_correlation_matrix(self, columns: list):
        """Method plots a correlation matrix of the specified columns in 'data_frame'.

        Parameters:
        -----------
        columns: list
            column names as list of strings of input variables.

        Returns:
        --------
        None
        """
        correlation_matrix = self.data_frame[columns].corr()
        mask = np.array(correlation_matrix)
        mask[np.tril_indices_from(mask)] = False
        a_x = plt.subplots(figsize=(15, 8))[1]
        sns.heatmap(
            correlation_matrix, mask=mask, vmax=0.8, square=True, annot=True, ax=a_x
        )
        a_x.set_title("Correlation matrix of attributes")
        plt.show()

    def plot(self):
        """
        Method allows you to pick a week number between 0 and 102 weeks.
        If the chosen number is not in the [0, 102] range the method raises a
        ValueError and warn the user of the allowed range. If everything is OK,
        the method displays the dataframe of the chosen week dataframe and
        plots the week on instances and count.

        Parameters:
        -----------
        None

        Raises:
        -------
        ValueError:
            If input 'x_input' is not an integer.
            If input 'x_input' is < 0.
            If input 'x_input' is > 102.

        Returns:
        --------
        None
        """
        x_input = input("Enter a week number between 0-102:")
        try:
            x_input = int(x_input)
        except:
            raise ValueError("Please insert a valid integer.")

        if x_input > 102:
            raise ValueError("Please select a week number up to 102!")

        if x_input < 0:
            raise ValueError("Please select a number over 0.")

        mask = self.data_frame["week"] == x_input
        y_axis = self.data_frame["count"][mask]
        x_axis = self.data_frame["instant"][mask]
        plt.figure(figsize=(4, 3))
        plt.scatter(x_axis, y_axis)
        title = "Week " + str(x_input)
        plt.title(title)
        plt.xlabel("Instant")
        plt.ylabel("Count")
        plt.tight_layout()
        plt.show()

        self.data_frame[self.data_frame["week"] == x_input]

    def plot_average_total_rentals(self):
        """Method plots a barchart where the x-axis is the month and the y-axis is
        the total average rentals.

        Parameters:
        -----------
        None

        Returns:
        --------
        None
        """
        y_values = []
        years = set(self.data_frame["year"])
        months = set(self.data_frame["month"])

        for month in months:
            res = 0
            for year in years:
                res += self.data_frame["count"][
                    (self.data_frame["year"] == year)
                    & (self.data_frame["month"] == month)
                ].sum()
            res /= len(years)
            y_values.append(res)

        sns.barplot(x=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], y=y_values).set(
            xlabel="Month", ylabel="Total Average Rentals"
        )

    def forecast(self, month: int):
        """
        Method selects all days from the dataframe corresponding to the month input integer
        and creates an "Average" and "Standard Deviation" array for the hours of an entire week
        which will be plotted.
        It further plots a fourtier transformation to find synchronicities in the data.

        Parameters:
        -----------
        month: integer
            month to be plotted, as integer 1-12
        Raises:
        ------
        ValueError:
            If input 'month' is < 1.
            If input 'month' is > 12.

        Returns:
        --------
        None
        """
        month = int(month)
        if month > 12 or month < 1:
            raise ValueError("Please select a month between 1-12!")

        temp_data_frame = self.data_frame[self.data_frame["month"] == month][
            ["datetime", "hr", "count"]
        ]
        temp_data_frame["weekday"] = [
            d.weekday() for d in pd.to_datetime(temp_data_frame.index)
        ]
        temp_data_frame.drop("datetime", axis=1, inplace=True)
        average_data_frame = temp_data_frame.groupby(by=["weekday", "hr"])[
            "count"
        ].mean()
        std_data_frame = temp_data_frame.groupby(by=["weekday", "hr"])["count"].std()

        months = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        ]
        title = "Expected weekly rentals in " + months[month - 1]
        labels = [
            "Mon 00:00",
            "Tue 00:00",
            "Wed 00:00",
            "Thu 00:00",
            "Fri 00:00",
            "Sat 00:00",
            "Sun 00:00",
            "Mon 00:00",
        ]

        x_range = np.arange(168)
        plt.figure(figsize=(10, 3))
        plt.style.use("seaborn")
        plt.plot(x_range, average_data_frame, label="Expected Rentals")
        plt.title(title)
        plt.fill_between(
            x_range,
            average_data_frame - std_data_frame,
            average_data_frame + std_data_frame,
            alpha=0.2,
        )
        plt.xlabel("Hours in a week")
        plt.ylabel("Rentals")
        plt.legend()
        plt.xticks(np.arange(min(x_range), max(x_range) + 10, 24), labels)

        plt.show()

        freqdomain = np.fft.rfft(average_data_frame)
        freqdays = np.linspace(0, 0.5, len(freqdomain))
        a_x = plt.subplots(figsize=(15, 4))[1]
        a_x.plot(1 / freqdays[1:-1], np.abs(freqdomain)[1:-1])
        a_x.set_xlabel("Hours needed for an event to repeat [Hours]")
        a_x.set_ylabel("Amplitude of event [AU]")
        a_x.set_ylim(bottom=0)
        plt.tight_layout()
        plt.show()
