==========================================
AdPro Assignment - Group_C - on Bike Sharing Dataset
==========================================

Diego Bellon 45637
Marc Serafin 44832
Sophie Schmitz 45718
Hannah Hoehnke 44536
Kevin Cabrera Fugardo 44406
Wiebke Wegener 44354

Nova SBE

=========================================
Background 
=========================================

Our company is participating in a two-day hackathon promoted to study the mobility of
bicycle users. They send our group, the best team of Data Scientists in the company's roster.
By promoting cycling awareness, our company expects to contribute to the green transition.

=========================================
Findings
=========================================

Our concluding findings the following:
    1. More bikes should be made available with
       increasing temperature and lower humidity.
    2. Fewer bikes at night after midnight
    3. More bike rentals during the summer months between May-September,
       with a peak in August at around 175.000 per month.
    4. Bike rentals seem to be cyclical with a high number of users
       renting bikes in the morning and again in the evening
       (most likely moving to and from work) with slight increases
       again during lunch hours.
       Weekends seem to follow a differnt patterns with only one peak
       during midday, following a steady concave function form.
Our suggestion is therefore to use the increases in bike rentals in
the monings and afternoons during the week to make as many bikes available
as possible per shop to rent out, and use the work hours for maintenance
and relocations. For weekends most shops should excect the highest demand
around midday.
The increase from around 70.000 to 175.000 during May-September
should be kept in mind, as well as the correlation with weather conditions.
Further research should be done in highly used work routes to investigate
if the rental influxes overlap to then better
position the rental shops. For example, there might be a higher demand of
rental bikes in residential districts in the morning,
and a higher demand in commercial districts at evening hours.

=========================================
Data Set
=========================================
The data can be accessed through 
(https://archive.ics.uci.edu/ml/machine-learning-databases/00275/Bike-Sharing-Dataset.zip).
Bike-sharing rental process is highly correlated to the environmental and seasonal settings.
For instance, weather conditions,
precipitation, day of week, season, hour of the day, etc. can affect the rental behaviors.
The core data set is related to the two-year historical log corresponding to
years 2011 and 2012 from Capital Bikeshare system, Washington D.C., USA
which is publicly available in http://capitalbikeshare.com/system-data.
The data was aggregated on two hourly and daily basis and then extracted and
added the corresponding weather and seasonal information.
Weather information are extracted from http://www.freemeteo.com.

=========================================
Associated tasks
=========================================

	- Class:
		Includes 7 methods (not including the __init__ method):

            1. set_df(df_path):
                attributes the 'data_frame' to the instance
            2. download_url(url, save_path):
                downloads the Bike-Sharing-Dataset as a .zip file in 'downloads' file
            3. extract_zip(file_name):
                extract_zip: reads the .csv data in .zip file
            4. create_correlation_matrix(correlation_attributes):
                create_correlation_matrix: plots a correlation matrix between the attributes given
            5. plot():
                allows input of specific week, which is then plotted and the selected data
                frame displayed
            6. plot_average_total_rentals():
                barplot expected rentals by month
            7. forecast(i_month):
                plots expected weekly rentals by hour for every month and
                synchronicities of these rentals through plotting a Fourtier transformation


=========================================
Files
=========================================

	- Readme.md
    - demo.ipynb
    - handler.py
    - Licence
    - Bike-Sharing-Dataset.zip (created in method)
	- hour.csv : bike sharing counts aggregated on hourly basis. Records: 17379 hours
        (pulled in method)
	- day.csv - bike sharing counts aggregated on daily basis. Records: 731 days
        (pulled in method)

=========================================
Dataset characteristics
=========================================
Both hour.csv and day.csv have the following fields, except hr which is not available in day.csv
	
	- instant: record index
	- dteday : date
	- season : season (1:springer, 2:summer, 3:fall, 4:winter)
	- yr : year (0: 2011, 1:2012)
	- mnth : month ( 1 to 12)
	- hr : hour (0 to 23)
	- holiday : weather day is holiday or not (extracted from http://dchr.dc.gov/page/holiday-schedule)
	- weekday : day of the week
	- workingday : if day is neither weekend nor holiday is 1, otherwise is 0.
	+ weathersit : 
		- 1: Clear, Few clouds, Partly cloudy, Partly cloudy
		- 2: Mist + Cloudy, Mist + Broken clouds, Mist + Few clouds, Mist
		- 3: Light Snow, Light Rain + Thunderstorm + Scattered clouds, Light Rain + Scattered clouds
		- 4: Heavy Rain + Ice Pallets + Thunderstorm + Mist, Snow + Fog
	- temp : Normalized temperature in Celsius. The values are divided to 41 (max)
	- atemp: Normalized feeling temperature in Celsius. The values are divided to 50 (max)
	- hum: Normalized humidity. The values are divided to 100 (max)
	- windspeed: Normalized wind speed. The values are divided to 67 (max)
	- casual: count of casual users
	- registered: count of registered users
	- cnt: count of total rental bikes including both casual and registered

=========================================
License
=========================================
handler.py file:

    GNU GENERAL PUBLIC LICENSE
    Version 3, 29 June 2007
    (See Licence)

Dataset:

    Use of this dataset in publications must be cited to the following publication:

    [1] Fanaee-T, Hadi, and Gama, Joao, 
    "Event labeling combining ensemble detectors and background knowledge", 
    Progress in Artificial Intelligence (2013): 
    pp. 1-15, Springer Berlin Heidelberg, doi:10.1007/s13748-013-0040-3.

    @article{
        year={2013},
        issn={2192-6352},
        journal={Progress in Artificial Intelligence},
        doi={10.1007/s13748-013-0040-3},
        title={Event labeling combining ensemble detectors and background knowledge},
        url={http://dx.doi.org/10.1007/s13748-013-0040-3},
        publisher={Springer Berlin Heidelberg},
        keywords={Event labeling; Event detection; Ensemble learning; Background knowledge},
        author={Fanaee-T, Hadi and Gama, Joao},
        pages={1-15}
    }

=========================================
Contact
=========================================
	
For further information about this project please contact 
Diego Bellon 45637@novasbe.pt
Hannah Hoehnke 44536@novasbe.pt
Kevin Cabrera 44406@novasbe.pt
Marc Serafin 44832@novasbe.pt
Sophie Schmitz 45718@novasbe.pt
Wiebke Wegener 44354@novasbe.pt
